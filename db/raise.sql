SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `raise` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE `raise` ;

-- -----------------------------------------------------
-- Table `raise`.`Roles`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `raise`.`Roles` (
  `id` INT NOT NULL ,
  `rolename` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `raise`.`Users`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `raise`.`Users` (
  `id` INT NOT NULL ,
  `idroles` INT NULL ,
  `username` VARCHAR(45) NULL ,
  `name` VARCHAR(45) NULL ,
  `password` VARCHAR(45) NULL ,
  `address` VARCHAR(45) NULL ,
  `phone` VARCHAR(45) NULL ,
  `email` VARCHAR(45) NULL ,
  `roles_idroles` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_users_roles` (`roles_idroles` ASC) ,
  CONSTRAINT `fk_users_roles`
    FOREIGN KEY (`roles_idroles` )
    REFERENCES `raise`.`Roles` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `raise`.`Files`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `raise`.`Files` (
  `id` INT NOT NULL ,
  `idusers` INT NULL ,
  `created` DATETIME NULL ,
  `path` VARCHAR(45) NULL ,
  `users_idusers` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_files_users1` (`users_idusers` ASC) ,
  CONSTRAINT `fk_files_users1`
    FOREIGN KEY (`users_idusers` )
    REFERENCES `raise`.`Users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `raise`.`Fundraisers`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `raise`.`Fundraisers` (
  `id` INT NOT NULL ,
  `idusers` INT NULL ,
  `frname` VARCHAR(45) NULL ,
  `directory` VARCHAR(45) NULL ,
  `supported` VARCHAR(45) NULL ,
  `payee` VARCHAR(45) NULL ,
  `about` TEXT NULL ,
  `summary` TEXT NULL ,
  `required` DECIMAL(9,2) NULL ,
  `collected` DECIMAL(9,2) NULL ,
  `status` VARCHAR(45) NULL ,
  `Users_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Fundraisers_Users1` (`Users_id` ASC) ,
  CONSTRAINT `fk_Fundraisers_Users1`
    FOREIGN KEY (`Users_id` )
    REFERENCES `raise`.`Users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `raise`.`Campaigns`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `raise`.`Campaigns` (
  `id` INT NOT NULL ,
  `idfundraisers` INT NULL ,
  `name` VARCHAR(255) NULL ,
  `summary` TEXT NULL ,
  `status` INT NULL ,
  `required` DECIMAL(9,2) NULL ,
  `Fundraisers_id` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_Campaigns_Fundraisers1` (`Fundraisers_id` ASC) ,
  CONSTRAINT `fk_Campaigns_Fundraisers1`
    FOREIGN KEY (`Fundraisers_id` )
    REFERENCES `raise`.`Fundraisers` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `raise`.`Messages`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `raise`.`Messages` (
  `id` INT NOT NULL ,
  `idusersfrom` INT NULL ,
  `idusersto` INT NULL ,
  `created` DATETIME NULL ,
  `message` TEXT NULL ,
  `users_idusers` INT NOT NULL ,
  `users_idusers1` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_messages_users1` (`users_idusers` ASC) ,
  INDEX `fk_messages_users2` (`users_idusers1` ASC) ,
  CONSTRAINT `fk_messages_users1`
    FOREIGN KEY (`users_idusers` )
    REFERENCES `raise`.`Users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_messages_users2`
    FOREIGN KEY (`users_idusers1` )
    REFERENCES `raise`.`Users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `raise`.`Stores`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `raise`.`Stores` (
  `id` INT NOT NULL ,
  `name` VARCHAR(45) NULL ,
  `url` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `raise`.`Donations`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `raise`.`Donations` (
  `id` INT NOT NULL ,
  `idusers` INT NULL ,
  `idfundraisers` INT NULL ,
  `amount` DECIMAL(9,2) NULL ,
  `users_idusers` INT NOT NULL ,
  `fundraisers_idfundraisers` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_donations_users1` (`users_idusers` ASC) ,
  INDEX `fk_donations_fundraisers1` (`fundraisers_idfundraisers` ASC) ,
  CONSTRAINT `fk_donations_users1`
    FOREIGN KEY (`users_idusers` )
    REFERENCES `raise`.`Users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_donations_fundraisers1`
    FOREIGN KEY (`fundraisers_idfundraisers` )
    REFERENCES `raise`.`Fundraisers` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `raise`.`Cashback`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `raise`.`Cashback` (
  `id` INT NOT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;


-- -----------------------------------------------------
-- Table `raise`.`Shopping`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `raise`.`Shopping` (
  `id` INT NOT NULL ,
  `idusers` INT NULL ,
  `idstores` INT NULL ,
  `amount` DECIMAL(9,2) NULL ,
  `users_idusers` INT NOT NULL ,
  `stores_idstores` INT NOT NULL ,
  PRIMARY KEY (`id`) ,
  INDEX `fk_shopping_users1` (`users_idusers` ASC) ,
  INDEX `fk_shopping_stores1` (`stores_idstores` ASC) ,
  CONSTRAINT `fk_shopping_users1`
    FOREIGN KEY (`users_idusers` )
    REFERENCES `raise`.`Users` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_shopping_stores1`
    FOREIGN KEY (`stores_idstores` )
    REFERENCES `raise`.`Stores` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
