<?php
return array(
  'module_paths' => array(
    realpath(__DIR__ . '/../modules'),
  ),
  'modules' => array(
    'Fundraiser',
    'Application',
    'User',
    'Campaign',
    'Admin'
  ),
  'module_listener_options' => array(
    'config_cache_enabled' => false,
    'cache_dir' => realpath(__DIR__ . '/../data/cache'),
  ),
);
