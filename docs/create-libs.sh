#!/usr/bin/env bash

sudo mkdir -p /usr/share/php
cd /usr/share/php

# create Zend Framework 2
if [ ! -d "zf2" ]; then
  sudo git clone https://github.com/zendframework/zf2.git zf2
  sudo ln -s zf2/library/Zend Zend
fi

# create Doctrine 2
if [ ! -d "doctrine2-dbal" ]; then
  sudo git clone git://github.com/doctrine/dbal.git doctrine2-dbal
fi
if [ ! -d "doctrine2-orm" ]; then
  sudo git clone git://github.com/doctrine/doctrine2.git doctrine2-orm
fi

if [ ! -d "Doctrine" ]; then
  sudo mkdir Doctrine
  sudo ln -s ../doctrine2-dbal/lib/Doctrine/DBAL Doctrine/DBAL
  sudo ln -s ../doctrine2-orm/lib/Doctrine/ORM Doctrine/ORM
fi
