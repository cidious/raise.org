<?php

namespace User\Model;

use Zend\Db\Table\AbstractTable;

class Users extends AbstractTable
{
    protected $_name = 'Users';

    public function getUser($id)
    {
      $id = (int) $id;
      $row = $this->fetchRow('id = ' . $id);
      if (!$row) {
        throw new \Exception("Could not find row $id");
      }
      return $row->toArray();
    }

    public function addUser($data)
    {
      $data['roles_idroles'] = 1;
      $id = $this->insert($data);
      return $id;
    }

    public function updateUser($data)
    {
      $data['roles_idroles'] = 1;
      if (isset($data['password']) && $data['password']=='') {
        unset($data['password']);
      }
      $this->update($data, 'id = ' . (int) $data['id']);
    }

    public function loginUser($email, $password, $keep)
    {
      if (!$email) return false;

      $row = $this->fetchRow(
        array('email = ?' => $email,
              'password = ?' => $password));

      if (!is_object($row)) return false;

      $arr = $row->toArray();
      if (isset($arr['id']) && $arr['id']>0) {
        // TODO: rework for Zend\Auth
//        $GLOBALS['auth']->
        $_SESSION['auth'] = $arr;

        // TODO: set persistent cookie for session
        if (!empty($keep)) {
          
        }

        return true;
      }

      return false;
    }

  public function logoutUser()
  {
    if (isset($_SESSION['auth'])) {
      unset($_SESSION['auth']);
    }
  }

  public function deleteUser($id)
  {
    $this->delete('id =' . (int) $id);
  }

  public function getUserByEmail($email)
  {
    $row = $this->fetchRow(
        array('email = ?' => $email)
    );
    return $row;
  }

}
