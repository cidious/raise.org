<?php

namespace User\Controller;

use Zend\Mvc\Controller\ActionController,
    User\Model\Users,
    Fundraiser\Model\Fundraisers;

class UserController extends ActionController
{
  protected $users, $fundraisers;

  public function indexAction()
  {
      return array();
  }

  public function setUsers(Users $users)
  {
    $this->users = $users;
    return $users;
  }
  public function setFundraisers(Fundraisers $fundraisers)
  {
    $this->fundraisers = $fundraisers;
    return $fundraisers;
  }

  public function registerAction()
  {
      $request = $this->getRequest();
      if ($request->isPost()) {
        $formData = $request->post()->toArray();
        // TODO: validator in Zend way
//        if ($form->isValid($formData)) {
        if ($this->isValidEmulator($formData)) {

          // TODO: move the validation code into validator
          if (empty($_POST['firstName'])) {
            // TODO: rework for Zend standard
            return $this->redirect()->toUrl('/admin/login?m=FN');
          }
          if (empty($_POST['lastName'])) {
            return $this->redirect()->toUrl('/admin/login?m=LN');
          }
          if (empty($_POST['email'])) {
            return $this->redirect()->toUrl('/admin/login?m=EM');
          }
          if ($_POST['email'] != $_POST['vEmail']) {
            return $this->redirect()->toUrl('/admin/login?m=EMS');
          }
          if (empty($_POST['password'])) {
            return $this->redirect()->toUrl('/admin/login?m=PWD');
          }
          if ($_POST['password'] != $_POST['vPassword']) {
            return $this->redirect()->toUrl('/admin/login?m=PWDS');
          }
          if (!preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $_POST['email'])) {
            return $this->redirect()->toUrl('/admin/login?m=EMWR');
          }
          if ($this->users->getUserByEmail($_POST['email'])) {
            return $this->redirect()->toUrl('/admin/login?m=EMAIL');
          }

//          $email = $form->getValue('email');

          $id = $this->users->addUser(
            $u = array(
              'firstName'=> $_POST['firstName'],
              'lastName' => $_POST['lastName'],
              'email'    => $_POST['email'],
              'password' => $_POST['password']
            )
          );
          $u['id'] = $id;

          $_SESSION['auth'] = $u;

          return $this->redirect()->toRoute('default', array(
            'controller' => 'admin',
            'action' => 'dashboard',
          ));
        }
      }
      return array();
  }

  function isValidEmulator($formData)
  {
    return true;
  }

  public function loginAction()
  {
    $request = $this->getRequest();
    if ($request->isPost()) {
      $formData = $request->post()->toArray();


      // TODO: convert to zend way
      if ($this->users->loginUser($_POST['email'], $_POST['password'])) {

        return $this->redirect()->toRoute('default', array(
          'controller' => 'admin',
          'action' => 'dashboard',
        ));
      } else {
        // TODO: rework for Zend standard
        return $this->redirect()->toUrl('/admin/login?m=WRONG');
      }
    }
    return $this->redirect()->toRoute('default', array(
          'controller' => 'admin',
          'action' => 'login',
        ));
  }

  public function logoutAction()
  {
    $this->users->logoutUser();
    return $this->redirect()->toRoute('default', array(
      'controller' => 'user',
      'action' => 'index',
    ));
  }

  public function infoAction()
  {
    $request = $this->getRequest();
    if ($request->isPost()) {
      $formData = $request->post()->toArray();
      
      $pwd = $formData['password']==''?$_SESSION['auth']['password']:$formData['password'];
      $this->users->updateUser($_SESSION['auth']['id'], $_SESSION['auth']['username'],
        $formData['email'], $pwd, $formData['name'], $formData['address'],
        $formData['phone']);

      $fundraiser = $this->fundraisers->getFundraiserByUser($_SESSION['auth']['id']);
      if (empty($fundraiser[0]) || empty($fundraiser[0]['id'])) {
        $this->fundraisers->addFundraiser($_SESSION['auth']['id'], $formData['frname'],
          $formData['directory'], $formData['required'], $formData['about'],
          $formData['summary']);

        //TODO: change to returned last id

      } else {

        $this->fundraisers->updateFundraiser($fundraiser[0]['id'], $formData['frname'],
          $formData['directory'], $formData['required'], $formData['about'],
          $formData['summary']);
      }

      return $this->redirect()->toRoute('default', array(
        'controller' => 'fundraiser',
        'action' => 'index',
      ));

    } else {
      if (empty($_SESSION['auth']['id'])) {
        return $this->redirect()->toRoute('default', array(
          'controller' => 'fundraiser',
          'action' => 'index',
        ));
      }
      $user = $this->users->getUser($_SESSION['auth']['id']);


      $fundraisers = $this->fundraisers->getFundraiserByUser($_SESSION['auth']['id']);


      $fundraiser = $this->fundraisers->getFundraiserByUser($_SESSION['auth']['id']);


      return array(
        'user' => $user,
        'fundraiser' => $fundraiser,
        'fundraisers' => $fundraisers,
        'title' => 'User Page'
      );
    }
  }
}
