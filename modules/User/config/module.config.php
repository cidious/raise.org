<?php
return array(
  'di' => array(
    'instance' => array(
      'alias' => array(
        'user' => 'User\Controller\UserController',
      ),
      'User\Controller\UserController' => array (
        'parameters' => array(
          'users' => 'User\Model\Users',
          'fundraisers' => 'Fundraiser\Model\Fundraisers'
        )
      ),
      'User\Model\Users' => array(
        'parameters' => array(
          'config' => 'Zend\Db\Adapter\Mysqli'
        )
      ),
      'Zend\Db\Adapter\Mysqli' => array(
          'parameters' => array(
              'config' => array(
                  'host' => 'localhost',
                  'username' => 'raise',
                  'password' => 'raise',
                  'dbname' => 'raise',
              ),
          ),
      ),
      'Zend\View\PhpRenderer' => array(
        'parameters' => array(
          'options'  => array(
            'script_paths' => array(
              'User' => __DIR__ . '/../views',
            ),
          ),
        ),
      ),
    ),
  ),
);
