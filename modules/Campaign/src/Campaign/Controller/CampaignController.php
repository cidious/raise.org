<?php

namespace Campaign\Controller;

use Zend\Mvc\Controller\ActionController,
    Campaign\Model\Campaigns,
    Campaign\Form\CampaignForm;

class CampaignController extends ActionController
{
  protected $campaigns;
    
  public function indexAction()
  {
    if (!empty($_GET['id'])) {
      $campaigns = $this->campaigns->getCampaign($_GET['id']);
    } else $campaigns = array();

    return array(
      'campaigns' => $campaigns
    );
  }

    public function addAction()
    {
      $form = new CampaignForm();
//      $form->submit->setLabel('Add');

      $request = $this->getRequest();
      if ($request->isPost()) {
        $formData = $request->post()->toArray();
        if ($form->isValid($formData)) {
          $fundraiser= $form->getValue('fundraiser');
          $name      = $form->getValue('name');
          $required  = $form->getValue('required');
          $summary   = $form->getValue('summary');
          $this->campaigns->addCampaign($fundraiser, $name, $summary, 1, $required);

          return $this->redirect()->toRoute('default', array(
             'controller' => 'fundraiser',
             'action' => 'edit'
          ), array(
             'id' => $fundraiser
          ));
        }
      } else {
        $fundraiser = $request->query()->get('fundraiser', 0);
        if ($fundraiser > 0) {
          $form->populate(array('fundraiser'=>$fundraiser));
        }
      }

      return array('form' => $form);
    }

    public function editAction()
    {
        $form = new CampaignForm();
//        $form->submit->setLabel('Edit');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $formData = $request->post()->toArray();
            if ($form->isValid($formData)) {
                $id = $form->getValue('id');
                $name      = $form->getValue('name');
                $required  = $form->getValue('required');
                $summary   = $form->getValue('summary');
                
                if ($this->campaigns->getCampaign($id)) {
                    $this->campaigns->updateCampaign($id, $name, $summary, 1, $required);
                }

                return $this->redirectToList();
            }
        } else {
            $id = $request->query()->get('id', 0);
            if ($id > 0) {
                $form->populate($this->campaigns->getCampaign($id));
            }
        }

        return array('form' => $form);
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $del = $request->post()->get('del', 'No');
            if ($del == 'Yes') {
                $id = (int) $request->post()->get('id');
                $this->campaigns->deleteCampaign($id);
            }

            // Redirect to list of campaigns
            return $this->redirectToList();
        }

        $id = $request->query()->get('id', 0);
        return array('campaign' => $this->campaigns->getCampaign($id));
    }
    
    public function setCampaigns(Campaigns $campaigns)
    {
        $this->campaigns = $campaigns;
        return $this;
    }
    
    protected function redirectToList()
    {
        // Redirect to list of campaigns
        return $this->redirect()->toRoute('default', array(
                'controller' => 'campaign',
                'action' => 'index',
            ));
    }
    
}