<?php

namespace Campaign\Model;

use Zend\Db\Table\AbstractTable;

class Campaigns extends AbstractTable
{
    protected $_name = 'Campaigns';

    public function getCampaign($id)
    {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row->toArray();
    }

    public function getCampaignByFundraiser($Id)
    {
      $Id = (int) $Id;
      $row = $this->fetchRow('Fundraisers_id = ' . $Id, 'id DESC', 1);
      if (!$row) {
        return null;
//        throw new \Exception("Could not find row $Id");
      }
      return $row->toArray();
    }

    public function addCampaign($data)
    {
/*      $data = array(
        'Fundraisers_id'=> $Fundraisers_id,
        'name'          => $name,
        'summary'       => $summary,
        'status'        => $status,
        'required'      => $required
      );*/
      return $this->insert($data);
    }

    public function updateCampaign($data)
    {
/*      $data = array(
        'name'          => $name,
        'summary'       => $summary,
        'status'        => $status,
        'required'      => $required
      );*/
      if (isset($data['id']) && is_numeric($data['id'])) {
        $id = $data['id'];
        unset($data['id']);
        $this->update($data, 'id = ' . (int) $id);
      }
    }

    public function deleteCampaign($id)
    {
        $this->delete('id =' . (int) $id);
    }

}
