<?php
namespace Campaign\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class CampaignForm extends Form
{
    public function init()
    {
        $this->setName('campaign');

        $id = new Element\Hidden('id');
        $id->addFilter('Int');
        $fundraiser = new Element\Hidden('fundraiser');
        $fundraiser->addFilter('Int');

        $name = new Element\Text('name');
        $name->setLabel('Name')
               ->setRequired(true)
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
               ->addValidator('NotEmpty');

        $goal = new Element\Text('required');
        $goal->setLabel('Goal')
              ->setRequired(false)
              ->addFilter('StripTags')
              ->addFilter('StringTrim');

        $summary = new Element\Textarea('summary');
        $summary->setLabel('Summary')
              ->setRequired(false);

        $submit = new Element\Submit('submit');
        $submit->setLabel('Save')->setAttrib('id', 'submitbutton');

        $this->addElements(array($id, $fundraiser, $name, $goal, $summary, $submit));
    }
}
