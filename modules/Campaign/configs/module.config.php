<?php

$default = array(
  'di' => array('instance' => array(
    'alias' => array(
      'campaign' => 'Campaign\Controller\CampaignController',
      'error' => 'Campaign\Controller\ErrorController',
    ),
    'Campaign\Controller\CampaignController' => array(
      'parameters' => array(
          'campaigns' => 'Campaign\Model\Campaigns',
      ),
    ),
    'Campaign\Model\Campaigns' => array(
      'parameters' => array(
          'config' => 'Zend\Db\Adapter\Mysqli',
    )),
    'Zend\Db\Adapter\Mysqli' => array(
      'parameters' => array(
        'config' => array(
            'host' => 'localhost',
            'username' => 'raise',
            'password' => 'raise',
            'dbname' => 'raise',
        ),
      ),
    ),
    'Zend\View\PhpRenderer' => array(
      'parameters' => array(
        'resolver' => 'Zend\View\TemplatePathStack',
        'options' => array(
            'script_paths' => array(
                'Campaign' => __DIR__ . '/../views',
            ),
        ),
      ),
    ),
  )),
);

// published environments
$production = $default;
$staging = $default;
$testing = $default;
$development = $default;

$config = compact('production', 'staging', 'testing', 'development');
return $config;
