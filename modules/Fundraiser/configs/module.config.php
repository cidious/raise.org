<?php

$default = array(
  'layout'          => 'layouts/layout.phtml',
  'di' => array('instance' => array(
    'alias' => array(
      'fundraiser' => 'Fundraiser\Controller\FundraiserController',
      'error' => 'Fundraiser\Controller\ErrorController',
    ),
    'Fundraiser\Controller\FundraiserController' => array(
      'parameters' => array(
        'fundraisers' => 'Fundraiser\Model\Fundraisers',
        'campaigns' => 'Campaign\Model\Campaigns'
      ),
    ),
    'Fundraiser\Model\Fundraisers' => array(
      'parameters' => array(
          'config' => 'Zend\Db\Adapter\Mysqli',
    )),
    'Zend\Db\Adapter\Mysqli' => array(
      'parameters' => array(
        'config' => array(
            'host' => 'localhost',
            'username' => 'raise',
            'password' => 'raise',
            'dbname' => 'raise',
        ),
      ),
    ),
    'Zend\View\PhpRenderer' => array(
      'parameters' => array(
        'resolver' => 'Zend\View\TemplatePathStack',
        'options' => array(
            'script_paths' => array(
                'Fundraiser' => __DIR__ . '/../views',
            ),
        ),
      ),
    ),
  )),
);

// published environments
$production = $default;
$staging = $default;
$testing = $default;
$development = $default;

$config = compact('production', 'staging', 'testing', 'development');
return $config;
