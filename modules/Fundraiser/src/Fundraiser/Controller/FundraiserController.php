<?php

namespace Fundraiser\Controller;

use Zend\Mvc\Controller\ActionController,
    Fundraiser\Model\Fundraisers,
    Fundraiser\Form\FundraiserForm,
    Campaign\Model\Campaigns;

class FundraiserController extends ActionController
{
  protected $fundraisers, $campaigns;
    
  public function rootAction()
  {
    $fundraisers = $this->fundraisers->getLastFundraisers(5);
    return array(
      'fundraisers' => $fundraisers
    );
  }

  public function indexAction()
  {
    if (!empty($_GET['id'])) {
      $fundraisers = $this->fundraisers->getFundraiser($_GET['id']);
    } else $fundraisers = array();

    return array(
      'fundraisers' => $fundraisers
    );
  }

  public function addAction()
  {
    $form = new FundraiserForm();
    $form->submit->setLabel('Add');

    $request = $this->getRequest();
    if ($request->isPost()) {
      $formData = $request->post()->toArray();
      if ($form->isValid($formData)) {
        $Users_id = $_SESSION['auth']['id'];
        $frname = $form->getValue('frname');
        $directory = $form->getValue('directory');
        $required = $form->getValue('required');
        $about = $form->getValue('about');
        $summary = $form->getValue('summary');

        $this->fundraisers->addFundraiser($Users_id, $frname, $directory,
          $required, $about, $summary);

        // Redirect to list of fundraisers
        return $this->redirectToList();
      }
    }

    return array('form' => $form);
  }

  public function editAction()
  {
    $form = new FundraiserForm();
    $form->submit->setLabel('Edit');

    $request = $this->getRequest();
    if ($request->isPost()) {
      $formData = $request->post()->toArray();
      if ($form->isValid($formData)) {
        $id = $form->getValue('id');
        $frname = $form->getValue('frname');
        $directory = $form->getValue('directory');
        $required = (float)$form->getValue('required');
        $about = $form->getValue('about');
        $summary = $form->getValue('summary');

        if ($this->fundraisers->getFundraiser($id)) {
            $this->fundraisers->updateFundraiser($id, $frname, $directory
            , $required, $about, $summary);
        }



/*        if ($_POST['addcampaign']=="1") {
          $name = $_POST['cpname'];
          $summary = $_POST['cpsummary'];
          $required = $_POST['cprequired'];
          $campaigns = $this->campaigns->getCampaignByFundraiser($id);
          if (count($campaigns)>0) {
            $c0 = $campaigns[0];
            $this->campaigns->updateCampaign($c0['id'],
              $id, $name,
              $summary, $c0['status'], $required);
          } else {
            $this->campaigns->addCampaign($id, $name, $summary,
              1, $required);
          }
        }*/



        // Redirect to list of fundraisers
        return $this->redirectToList();
      }
    } else {
      $id = $request->query()->get('id', 0);
      if ($id > 0) {
        $form->populate($fundraiser=$this->fundraisers->getFundraiser($id));
        $campaigns = $this->campaigns->getCampaignByFundraiser($id);
        if (count($campaigns)>0) {
          $campaign = $campaigns[0];
        } else $campaign = array();
      }
    }

    return array(
      'form' => $form,
      'fundraiser' => $fundraiser,
      'campaign' =>   $campaign,
      'campaigns' => $campaigns
    );
  }

  public function deleteAction()
  {
    $request = $this->getRequest();
    if ($request->isPost()) {
      $del = $request->post()->get('del', 'No');
      if ($del == 'Yes') {
        $id = (int) $request->post()->get('id');
        $this->fundraisers->deleteFundraiser($id);
      }

      // Redirect to list of fundraisers
      return $this->redirectToList();
    }

    $id = $request->query()->get('id', 0);
    return array('fundraiser' => $this->fundraisers->getFundraiser($id));
  }
    
  public function setFundraisers(Fundraisers $fundraisers)
  {
      $this->fundraisers = $fundraisers;
      return $this;
  }
  public function setCampaigns(Campaigns $campaigns)
  {
      $this->campaigns = $campaigns;
      return $this;
  }

  protected function redirectToList()
  {
      // Redirect to list of fundraisers
      return $this->redirect()->toRoute('default', array(
              'controller' => 'user',
              'action' => 'info',
          ));
  }


  protected function howtouseAction()
  {
    return array();
  }
  protected function aboutAction()
  {
    return array();
  }
  protected function blogAction()
  {
    return array();
  }
  protected function merchantsAction()
  {
    return array();
  }
  protected function couponsAction()
  {
    return array();
  }

}