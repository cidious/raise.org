<?php
namespace Fundraiser\Form;

use Zend\Form\Form,
    Zend\Form\Element;

class FundraiserForm extends Form
{
    public function init()
    {
        $this->setName('fundraiser');

        $id = new Element\Hidden('id');
        $id->addFilter('Int');

        $frname = new Element\Text('frname');
        $frname->setLabel('Name')
               ->setRequired(true)
               ->addFilter('StripTags')
               ->addFilter('StringTrim')
               ->addValidator('NotEmpty');

        $directory = new Element\Text('directory');
        $directory->setLabel('Directory')
              ->setRequired(true)
              ->addFilter('StripTags')
              ->addFilter('StringTrim')
              ->addValidator('NotEmpty');

        $required = new Element\Text('required');
        $required->setLabel('Goal')
              ->setRequired(true)
              ->addFilter('StripTags')
              ->addFilter('StringTrim')
              ->addValidator('NotEmpty');

        $about = new Element\Text('about');
        $about->setLabel('About')
              ->setRequired(false)
              ->addFilter('StripTags')
              ->addFilter('StringTrim');

        $summary = new Element\Textarea('summary');
        $summary->setLabel('Summary')
              ->setRequired(false)
              ->addFilter('StripTags')
              ->addFilter('StringTrim');

        $submit = new Element\Submit('submit');
        $submit->setAttrib('id', 'submitbutton');

        $this->addElements(array($id, $frname, $directory, $required, $about, $summary, $submit));
    }
}
