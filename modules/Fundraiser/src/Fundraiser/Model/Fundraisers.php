<?php

namespace Fundraiser\Model;

use Zend\Db\Table\AbstractTable;

class Fundraisers extends AbstractTable
{
  protected $_name = 'Fundraisers';

  public function getFundraiser($id, $Users_id=false)
  {
    $id = (int) $id;
    if (!$Users_id) {
      $Users_id = $_SESSION['auth']['id'];
    }
    $row = $this->fetchRow('id='.$id." and Users_id=".$Users_id);
    if (!$row) {
      throw new \Exception("Could not find row $id");
    }
    return $row->toArray();
  }

  public function getFundraiserByUser($userId)
  {
    $userId = (int) $userId;
    $row = $this->fetchAll('Users_id = '.$userId, 'id DESC');
    if (!$row) {
      throw new \Exception("Could not find row $userId");
    }
    return $row->toArray();
  }

  public function getLastFundraisers($number)
  {
    $number = (int) $number;
//    $row = $this->fetchAll('Users_id = ' . $userId);
    $rows = $this->fetchAll($this->select()->where('status <> ?', 'D')
      ->order('id DESC')
      ->limit($number, 0));
    if (!$rows) {
      throw new \Exception("Could not find rows");
    }
    $arr = $rows->toArray();
    foreach ($arr as $k=>$a) {
      $arr[$k]['short'] = substr(strip_tags($a['summary']), 0, 150);
      if (strip_tags($a['summary']) != substr(strip_tags($a['summary']), 0, 150)) {
        $arr[$k]['short'] .= '...';
      }
    }
    return $arr;
  }

  public function addFundraiser($data)
  {
    return $this->insert($data);
  }

  public function updateFundraiser($data, $userId)
  {
    if (isset($data['id']) && is_numeric($data['id'])) {
      $id = $data['id'];
      unset($data['id']);

      $this->update($data, 'id = '.(int)$id.' and Users_id='.$userId);
    }
  }

  public function deleteFundraiser($id)
  {
      $this->delete('id =' . (int) $id);
  }

}
