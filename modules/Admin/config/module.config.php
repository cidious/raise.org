<?php
return array(
  'di' => array(
    'instance' => array(
      'alias' => array(
        'admin' => 'Admin\Controller\AdminController',
      ),
      'Admin\Controller\AdminController' => array (
        'parameters' => array(
          'users' => 'User\Model\Users',
          'fundraisers' => 'Fundraiser\Model\Fundraisers',
          'campaigns' => 'Campaign\Model\Campaigns',
          'layout' => 'Zend\Layout\Layout'
        )
      ),
      'Admin\Model\Admins' => array(
        'parameters' => array(
          'config' => 'Zend\Db\Adapter\Mysqli'
        )
      ),
      'Zend\Db\Adapter\Mysqli' => array(
          'parameters' => array(
              'config' => array(
                  'host' => 'localhost',
                  'username' => 'raise',
                  'password' => 'raise',
                  'dbname' => 'raise',
              ),
          ),
      ),
      'Zend\View\PhpRenderer' => array(
        'parameters' => array(
          'options'  => array(
            'script_paths' => array(
              'Admin' => __DIR__ . '/../views',
            ),
          ),
        ),
      ),
    ),
  ),
);
