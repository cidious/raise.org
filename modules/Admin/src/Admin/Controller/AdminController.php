<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\ActionController,
    Admin\Model\Admins,
    User\Model\Users,
    Fundraiser\Model\Fundraisers,
    Campaign\Model\Campaigns,
    Zend\Layout\Layout,
    Zend\Http\Response;

class AdminController extends ActionController
{
  protected $users, $fundraisers, $campaigns, $layout;
  protected $messages = array(
    'WRONG' => array(2,"You entered a wrong login or password"),
    'EMAIL' => array(1,"The provided E-mail is already registered"),
    'FN'    => array(1,"First Name is empty"),
    'LN'    => array(1,"Last Name is empty"),
    'EM'    => array(1,"E-mail is empty"),
    'EMS'   => array(1,"E-mails did not match"),
    'PWD'   => array(1,"Password is empty"),
    'PWDS'  => array(1,"Passwords did not match"),
    'EMWR'  => array(1,"Email is not valid"),
  );

//  public function init()
//  public function preDispatch()
//  {
//    if (empty($_SESSION['auth'])) {
//      $this->getResponse()->headers()->addHeaderLine('Location', '/admin/login');
//      header('Location: /admin/login'); exit;
//    }

/*    $auth = Zend_Auth::getInstance();
    $userAuth = $auth->getStorage()->read();

    if (isset($userAuth->Id)) {
      $this->_redirect('/');
    }*/
//  }

  public function setUsers(Users $users)
  {
    $this->users = $users;
    return $users;
  }
  public function setFundraisers(Fundraisers $fundraisers)
  {
    $this->fundraisers = $fundraisers;
    return $fundraisers;
  }
  public function setCampaigns(Campaigns $campaigns)
  {
    $this->campaigns = $campaigns;
    return $campaigns;
  }
  public function setLayout(Layout $layout)
  {
    $this->layout = $layout;
    return $layout;
  }

  public function indexAction()
  {
    return array();
  }

  private $types = array(
    'nonprofit' => '501(c) Nonprofit',
    'education' => 'Education Institution',
    'personal'  => 'Personal'
  );
  private function getType($type)
  {
    return $this->types[$type];
  }

  private function checkLogged()
  {
    if (empty($_SESSION['auth']) || empty($_SESSION['auth']['id'])) {
      return $this->redirect()->toRoute('default', array(
        'controller' => 'admin',
        'action' => 'login',
      ));
    } else {
      return true;
    }
  }

  public function dashboardAction()
  {
    if (($logret = $this->checkLogged())!==true) {
      return $logret;
    }

    $fundraisers = $this->fundraisers->getFundraiserByUser($_SESSION['auth']['id']);

    return array(
      'fundraisers' => $fundraisers,
      'title'       => 'My Dashboard',
      'types'       => $this->types
    );
  }

  private function open_image($file)
  {
    $size = getimagesize($file);
    switch ($size["mime"]) {
      case "image/jpeg":
        $im = imagecreatefromjpeg($file); //jpeg file
        break;
      case "image/gif":
        $im = imagecreatefromgif($file); //gif file
        break;
      case "image/png":
        $im = imagecreatefrompng($file); //png file
        break;
      default:
        $im = false;
        break;
    }
    return $im;
  }
  private function createAvatar($fileName, $newName, $newWidth=0, $newHeight=0, $sharpen=0, $blur=0)
  {
    ini_set('memory_limit', '100M');
    ini_set("gd.jpeg_ignore_warning", 1);
    $imageSource = $this->open_image($fileName);

    if ($imageSource === false) {
      throw new \Exception("That's not an image file", 666);
      return false;
    }

    $w = imagesx($imageSource);
    $h = imagesy($imageSource);

    //calculate new image dimensions (preserve aspect)
    if ($newWidth && !$newHeight) {
      $new_w = $newWidth;
      $new_h = $new_w * ($h / $w);
    } elseif ($newHeight && !$newWidth) {
      $new_h = $newHeight;
      $new_w = $new_h * ($w / $h);
    } else {
      $new_w = $newWidth ? $newWidth : 180;
      $new_h = $newHeight ? $newHeight : 180;
      if (($w / $h) > ($new_w / $new_h)) {
        $new_h = $new_w * ($h / $w);
      } else {
        $new_w = $new_h * ($w / $h);
      }
    }

    $imageEffect = ImageCreateTrueColor($new_w, $new_h);
    imagecopyResampled($imageEffect, $imageSource, 0, 0, 0, 0, $new_w, $new_h, $w, $h);

    //effects
    if ($blur) {
      $lv = $blur;
      for ($i = 0; $i < $lv; $i++) {
        $matrix = array(array(1, 1, 1), array(1, 1, 1), array(1, 1, 1));
        $divisor = 9;
        $offset = 0;
        imageconvolution($imageEffect, $matrix, $divisor, $offset);
      }
    }
    if ($sharpen) {
      $lv = $sharpen;
      for ($i = 0; $i < $lv; $i++) {
        $matrix = array(array(-1, -1, -1), array(-1, 16, -1), array(-1, -1, -1));
        $divisor = 8;
        $offset = 0;
        imageconvolution($imageEffect, $matrix, $divisor, $offset);
      }
    }

    imagejpeg($imageEffect, $newName);
    imagedestroy($imageEffect);
    imagedestroy($imageSource);

    if (is_readable($newName) && filesize($newName)>0) {
      return true;
    } else {
      return false;
    }
  }

  public function editProfileAction()
  {
    if (($logret = $this->checkLogged())!==true) {
      return $logret;
    }

    $request = $this->getRequest();
    if ($request->isPost()) {
      $formData = $request->post()->toArray();

      $data = array(
        'firstName' => $formData['firstName'],
        'lastName'  => $formData['lastName'],
        'email'     => $formData['email'],
        'address1'  => $formData['address1'],
        'address2'  => $formData['address2'],
        'city'      => $formData['city'],
        'state'     => $formData['state'],
        'zip'       => $formData['zip'],
        'phone'     => $formData['phone'],
        'password'  => $formData['password'],

        'id'        => $_SESSION['auth']['id']
      );

      if (!empty($formData['avatarTemp']) && is_readable($formData['avatarTemp'])
      && filesize($formData['avatarTemp']) > 0) {
        rename($formData['avatarTemp'],
               $newAvatar=str_replace('temp_', '', $formData['avatarTemp']));
        $_SESSION['auth']['avatar'] = $data['avatar'] = $newAvatar;
      }

      $this->users->updateUser($data);
      $_SESSION['auth']['firstName'] = $formData['firstName'];
      $_SESSION['auth']['lastName'] = $formData['lastName'];

/*      return $this->redirect()->toRoute('default', array(
        'controller' => 'admin',
        'action' => 'editProfile',
      ));*/
      return $this->redirect()->toUrl('/admin/editProfile?m=OK');

    } else {
      $user = $this->users->getUser($_SESSION['auth']['id']);
      return array(
        'user' => $user,
        'title' => 'Edit Profile'
      );
    }
  }

  function isValidEmulator($formData)
  {
    return true;
  }

  public function loginAction()
  {
    $request = $this->getRequest();
    if ($request->isPost()) {
      $formData = $request->post()->toArray();

      if ($this->users->loginUser($_POST['email'], $_POST['password'], $_POST['keep'])) {

        return $this->redirect()->toRoute('default', array(
          'controller' => 'admin',
          'action' => 'dashboard',
        ));
      }

    } else {
      if (empty($_SESSION['auth'])) {
        return array(
          'message' => !empty($_GET['m'])&&isset($this->messages[$_GET['m']])?
            $this->messages[$_GET['m']][1]:'',
          'side' => $this->messages[$_GET['m']][0]
        );
      } else {
        return $this->redirect()->toRoute('default', array(
          'controller' => 'admin',
          'action' => 'dashboard',
        ));
      }
    }
  }

  public function logoutAction()
  {
    $this->users->logoutUser();
    return $this->redirect()->toRoute('default', array(
      'controller' => 'fundraiser',
      'action' => 'index',
    ));
  }

  public function createFundraiserAction()
  {
    if (($logret = $this->checkLogged())!==true) {
      return $logret;
    }

    $request = $this->getRequest();
    if ($request->isPost()) {
      $formData = $request->post()->toArray();

      $data = array(
        'Users_id'  => $_SESSION['auth']['id'],
        'type'      => $formData['type'],
        'frname'    => $formData['frname'],
        'directory' => $formData['directory'],
        'payee'     => $formData['payee'],
        'about'     => $formData['about'],
        'firstName' => $formData['firstName'],
        'lastName'  => $formData['lastName'],
        'ssn'       => preg_replace('/[^\d]/', '', $formData['ssn']),
        'paymentMethod'=> $formData['paymentMethod'],
        'city'      => $formData['city'],
        'state'     => $formData['state'],
        'zip'       => preg_replace('/[^\d]/', '', $formData['zip']),
        'phone'     => $formData['phone'],
        'address1'  => $formData['address1'],
        'address2'  => $formData['address2'],
        'status'    => 'U'
      );

/*      if ($img=$this->processImage('avatar', 125, 'fr', false)) {
        $data['avatar'] = $img;
      }*/
      if (!empty($formData['avatarTemp']) && is_readable($formData['avatarTemp'])
      && filesize($formData['avatarTemp']) > 0) {
        rename($formData['avatarTemp'],
               $newAvatar=str_replace('temp_', '', $formData['avatarTemp']));
        $data['avatar'] = $newAvatar;
      }

      $id = $this->fundraisers->addFundraiser($data);

      $formData['cName'] = trim($formData['cName']);
      $formData['cGoal'] = (float)$formData['cGoal'];
      if ($formData['cName'] != '' || $formData['cGoal'] > 0) {
        $dataCampaign = array(
          'name'          => $formData['cName'],
          'summary'       => $formData['cDescription'],
          'required'      => $formData['cGoal'],
          'status'        => 1,
          'Fundraisers_id'=> $id
        );
        $this->campaigns->addCampaign($dataCampaign);
      }

      return $this->redirect()->toRoute('default', array(
        'controller' => 'admin',
        'action' => 'dashboard',
      ));
    }
    return array(
      'edit'  => false,
      'types' => $this->types
    );
  }

  public function editFundraiserAction()
  {
    if (($logret = $this->checkLogged())!==true) {
      return $logret;
    }

    $request = $this->getRequest();
    if ($request->isPost()) {
      $formData = $request->post()->toArray();

      $data = array(
        'Users_id'  => $_SESSION['auth']['id'],
        'type'      => $formData['type'],
        'frname'    => $formData['frname'],
        'directory' => $formData['directory'],
        'payee'     => $formData['payee'],
        'about'     => $formData['about'],
        'firstName' => $formData['firstName'],
        'lastName'  => $formData['lastName'],
        'ssn'       => preg_replace('/[^\d]/', '', $formData['ssn']),
        'paymentMethod'=> $formData['paymentMethod'],
        'city'      => $formData['city'],
        'state'     => $formData['state'],
        'zip'       => preg_replace('/[^\d]/', '', $formData['zip']),
        'phone'     => $formData['phone'],
        'address1'  => $formData['address1'],
        'address2'  => $formData['address2'],
        'id'        => $formData['id']
      );

/*      if ($img=$this->processImage('avatar', 125, 'fr', false)) {
        $data['avatar'] = $img;
      }*/
      if (!empty($formData['avatarTemp']) && is_readable($formData['avatarTemp'])
      && filesize($formData['avatarTemp']) > 0) {
        rename($formData['avatarTemp'],
               $newAvatar=str_replace('temp_', '', $formData['avatarTemp']));
        $data['avatar'] = $newAvatar;
      }

      $this->fundraisers->updateFundraiser($data, $_SESSION['auth']['id']);

      $formData['cName'] = trim($formData['cName']);
      $formData['cGoal'] = (float)$formData['cGoal'];
      if ($formData['cName'] != '' || $formData['cGoal'] > 0) {
        $dataCampaign = array(
          'name'          => $formData['cName'],
          'summary'       => $formData['cDescription'],
          'required'      => $formData['cGoal'],
        );
        if ($campaign=$this->campaigns->getCampaignByFundraiser($formData['id'])) {
          $dataCampaign['id'] = $campaign['id'];
          $this->campaigns->updateCampaign($dataCampaign);
        } else {
          $dataCampaign['Fundraisers_id'] = $formData['id'];
          $dataCampaign['status'] = 1;
          $this->campaigns->addCampaign($dataCampaign);
        }
      }

      return $this->redirect()->toRoute('default', array(
        'controller' => 'admin',
        'action' => 'dashboard',
      ));
      
    } else {
      $id = (int)$_GET['id'];
      return array(
        'fr'   => $this->fundraisers->getFundraiser($id),
        'cp'   => $this->campaigns->getCampaignByFundraiser($id),
        'edit' => true,
        'types'=> $this->types
      );
    }

  }

  public function fileUploadAction()
  {
    try {
      if ($img=$this->processImage('avatar', 139, 'temp_fr', false)) {
        echo $img;
      } else {
        echo 'false';
      }
    } catch (\Exception $e) {
      if (($code=$e->getCode()) == 666)
      echo $code;
    }

    $this->layout->disableLayout();
//    $this->_helper->layout()->disableLayout();
//    $this->_helper->viewRenderer->setNoRender(true);
/*    return array(
      'OK' => 'OKK'
    );*/

    // TODO: remove exit after fixing the disablelayout for zend2
    exit;
  }
  public function fileUploadProfileAction()
  {
    try {
      if ($img=$this->processImage('avatar', 186, 'temp_user', false)) {
        echo $img;
      } else {
        echo 'false';
      }
    } catch (\Exception $e) {
      if (($code=$e->getCode()) == 666)
      echo $code;
    }

    $this->layout->disableLayout();

    // TODO: the same as above
    exit;
  }

  private function processImage($filesIndex, $width, $prefix, $oldImage)
  {
    if (!empty($_FILES[$filesIndex]) && $_FILES[$filesIndex]['error']==0) {
      $random = time();
      $avatarName = "img/avatars/{$prefix}_".$_SESSION['auth']['id'].'_'.$random.".jpg";
      if ($this->createAvatar($_FILES[$filesIndex]['tmp_name'], $avatarName, $width)) {
        if ($oldImage && is_file($oldImage)) {
          @unlink($oldImage);
        }
      } else return false;
      return $avatarName;
    }
    return false;
  }

}
