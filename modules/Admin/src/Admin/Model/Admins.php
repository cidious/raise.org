<?php

namespace Admin\Model;

use Zend\Db\Table\AbstractTable;

class Admins extends AbstractTable
{
    protected $_name = 'Users';

    public function getUser($id)
    {
        $id = (int) $id;
        $row = $this->fetchRow('id = ' . $id);
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row->toArray();
    }

    public function addUser($username, $email, $password)
    {
        $data = array(
          'username' => $username,
          'email'    => $email,
          'password' => $password,
          'roles_idroles'=>1
        );
        $this->insert($data);
    }

    public function updateUser($id, $username, $email, $password, $name, $address, $phone)
    {
        $data = array(
          'username' => $username,
          'email'    => $email,
          'password' => $password,
          'name'     => $name,
          'address'  => $address,
          'phone'    => $phone,
          'roles_idroles'=>1
        );
        $this->update($data, 'id = ' . (int) $id);
    }

    public function loginUser($username, $password)
    {
//      $row = $this->fetchRow("username = '" . mysql_escape_string($username)
//      ." and password = '".mysql_escape_string($password)."'");

      $row = $this->fetchRow(
        array('username = ?'  => $username,
              'password = ?' => $password));

      $arr = $row->toArray();
      if (isset($arr['id']) && $arr['id']>0) {
        // TODO: rework for Zend\Auth
//        $GLOBALS['auth']->
        $_SESSION['auth'] = $arr;
        return true;
      }

      return false;
    }

  public function logoutUser()
  {
    if (isset($_SESSION['auth'])) {
      unset($_SESSION['auth']);
    }
  }

  public function deleteUser($id)
  {
      $this->delete('id =' . (int) $id);
  }

}
